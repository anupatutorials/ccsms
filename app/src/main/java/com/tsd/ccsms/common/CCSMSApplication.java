package com.tsd.ccsms.common;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.tsd.ccsms.BuildConfig;

public class CCSMSApplication extends Application {

    // Constants
    private static final String TAG = CCSMSApplication.class.getSimpleName();

    // Objects
    /*private DaoSession daoSession;*/

    @Override
    public void onCreate() {
        super.onCreate();

        /*DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "mdms-furuno-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();*/

        // Stetho for local DB view and network monitor
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);//chrome://inspect/#devices
        }
    }

    /*public DaoSession getDaoSession() {
        return daoSession;
    }*/

}
