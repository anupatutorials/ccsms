package com.tsd.ccsms.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.tsd.ccsms.R;
import com.tsd.ccsms.common.BaseActivity;
import com.tsd.ccsms.config.AppConst;
import com.tsd.ccsms.dtos.ScannedTicket;
import com.tsd.ccsms.dtos.response.BeanTicketValidate;
import com.tsd.ccsms.interfaces.listeners.RxResponseListener;
import com.tsd.ccsms.retrofit.TicketValidateRx;
import com.tsd.ccsms.utils.AppUtil;
import com.tsd.ccsms.utils.MyWebViewClient;
import com.tsd.ccsms.viewModels.MainActivityViewModel;

import retrofit2.Response;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    // Constants
    public static final String TAG = MainActivity.class.getSimpleName();

    // UI-Elements
    private WebView mWvWebView;
    private LinearLayout mLlProgressBar;
    private FloatingActionMenu mFloatingActionMenu;
    private FloatingActionButton mFabAbout, mFabTicketValidation, mFabRefresh;

    // Objects
    private boolean mBackPressedToExitOnce = false;
    private MainActivityViewModel mViewModel;
    private AlertDialog mPermissionSettingsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        /*
         * dev.akperera+2@gmail.com
         * Icbt1234
         * */
    }

    private void initView() {
        mViewModel = new MainActivityViewModel(MainActivity.this);

        mLlProgressBar = findViewById(R.id.activity_main_ll_progress_bar);

        mFloatingActionMenu = findViewById(R.id.activity_main_floating_action_menu);
        mFabAbout = findViewById(R.id.activity_main_fab_about);
        mFabTicketValidation = findViewById(R.id.activity_main_fab_ticket_validation);
        mFabRefresh = findViewById(R.id.activity_main_fab_refresh);

        mFabAbout.setOnClickListener(this);
        mFabTicketValidation.setOnClickListener(this);
        mFabRefresh.setOnClickListener(this);

        mWvWebView = findViewById(R.id.activity_main_wv_web_view);
        WebSettings webSettings = mWvWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWvWebView.setWebViewClient(new MyWebViewClient(mLlProgressBar));

        mWvWebView.loadUrl("http://app.securedserver.xyz/home");
    }

    @Override
    public void onBackPressed() {
        if (mFloatingActionMenu.isOpened()) {
            mFloatingActionMenu.close(true);
        }
        if (mBackPressedToExitOnce) {
            super.onBackPressed();
        } else {
            this.mBackPressedToExitOnce = true;
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBackPressedToExitOnce = false;
                }
            }, AppConst.BACK_PRESSED_DELAY);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWvWebView.canGoBack()) {
            mWvWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_main_fab_about:
                AppUtil.startActivity(MainActivity.this, AboutActivity.class);
                break;
            case R.id.activity_main_fab_ticket_validation:
                ticketValidationClickEvent();
                break;
            case R.id.activity_main_fab_refresh:
                mWvWebView.loadUrl("http://app.securedserver.xyz/home");
                break;
            default:
                break;
        }

        mFloatingActionMenu.close(true);
    }

    private void ticketValidationClickEvent() {
        mViewModel.requestRuntimePermissionForAccessCamera();
        mViewModel.getLiveCheckCameraPermission().observe(MainActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean permissionGranted) {
                if (permissionGranted) {
                    AppUtil.startActivity(MainActivity.this, TicketValidationActivity.class);
                } else { // Permanently permission denied
                    if (mPermissionSettingsDialog == null) {
                        mPermissionSettingsDialog = createShowSettingsDialog();
                    }
                    mPermissionSettingsDialog.show();
                }
            }
        });
    }

    private AlertDialog createShowSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppUtil.openPackageSettings(MainActivity.this);
            }
        });
        builder.setCancelable(false);
        return builder.create();
    }

}
