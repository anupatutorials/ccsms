package com.tsd.ccsms.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.tsd.ccsms.R;
import com.tsd.ccsms.utils.AppUtil;
import com.tsd.ccsms.viewModels.SplashActivityViewModel;


public class SplashActivity extends AppCompatActivity {

    // Constants
    private static final String TAG = SplashActivity.class.getSimpleName();

    // UI Components

    //Objects
    private boolean mTabletDevice;
    private SplashActivityViewModel mViewModel;
    private AlertDialog mSettingsDialog;


    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTabletDevice = getResources().getBoolean(R.bool.isTablet);
        if (mTabletDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_splash);

        initView();
    }

    private void initView() {

        mViewModel = new SplashActivityViewModel(SplashActivity.this);
        mViewModel.initViewModel();

        mViewModel.getLiveIsSignedIn().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isSignedIn) {
                if (isSignedIn) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    AppUtil.startActivityWithExtra(SplashActivity.this, intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    AppUtil.startActivityWithExtra(SplashActivity.this, intent);
                }
            }
        });

        mViewModel.getLiveShowSettings().observe(SplashActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean showSettings) {
                if (mSettingsDialog == null) {
                    mSettingsDialog = createShowSettingsDialog();
                }
                if (showSettings) {
                    mSettingsDialog.show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mTabletDevice) {
            mViewModel.requestMultiplePermissions();
        } else {
            final AlertDialog dialog = new AlertDialog.Builder(SplashActivity.this).create();
            AppUtil.showCustomStandardAlert(dialog, SplashActivity.this,
                    getResources().getString(R.string.alert_text),
                    getResources().getString(R.string.device_not_compatible_text),
                    getResources().getDrawable(R.drawable.ic_close),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            System.exit(1);
                        }
                    }, getResources().getString(R.string.ok_text), false);
        }
    }

    private AlertDialog createShowSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppUtil.openPackageSettings(SplashActivity.this);
            }
        });
        builder.setCancelable(false);
        return builder.create();
    }
}
