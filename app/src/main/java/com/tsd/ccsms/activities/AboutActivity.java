package com.tsd.ccsms.activities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.tsd.ccsms.R;
import com.tsd.ccsms.common.BaseActivity;

public class AboutActivity extends BaseActivity {

    // Constants
    private static final String TAG = AboutActivity.class.getSimpleName();

    // UI Components
    private TextView mTvAppVersionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        setToolbar(getResources().getString(R.string.about_activity_title), AboutActivity.this);
        // setDrawer();
        initView();
        setVersionNumber();
    }

    //Toolbar back button pressed
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initView() {
        mTvAppVersionText = findViewById(R.id.activity_about_tv_app_version_text);
    }

    // Set version number
    private void setVersionNumber() {
        try {
            PackageManager manager = AboutActivity.this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(AboutActivity.this.getPackageName(), 0);
            String version = info.versionName;
            mTvAppVersionText.setText("CCSMA V" + version + "\nCentralized Cinema Sales Monitoring Solution");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
