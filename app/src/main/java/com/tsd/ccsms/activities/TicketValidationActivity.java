package com.tsd.ccsms.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.tsd.ccsms.R;
import com.tsd.ccsms.common.BaseActivity;
import com.tsd.ccsms.config.AppConst;
import com.tsd.ccsms.dtos.ProcessResult;
import com.tsd.ccsms.dtos.ScannedTicket;
import com.tsd.ccsms.utils.AppUtil;
import com.tsd.ccsms.viewModels.TicketValidationActivityViewModel;

import java.io.IOException;
import java.lang.reflect.Field;

public class TicketValidationActivity extends BaseActivity implements View.OnClickListener {

    // Constants
    private static final String TAG = TicketValidationActivity.class.getSimpleName();

    // UI-Components
    private RelativeLayout mRlBaseView;
    private SurfaceView mSvCameraView;
    private TextView mTvScannedValue;
    private ImageView mIvRefresh, mIvFlash;

    // Objects
    private TicketValidationActivityViewModel mViewModel;
    private Dialog mProgressDialog;
    private CameraSource mCameraSource;
    private Camera mCamera;
    private boolean mFlashMode = false;
    private int showedCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_validation);

        setToolbar(getResources().getString(R.string.ticket_validation_activity_title), TicketValidationActivity.this);
        initView();
        setUpScanner();
    }

    private void initView() {
        mViewModel = new TicketValidationActivityViewModel(TicketValidationActivity.this);
        mViewModel.initViewModel();

        mViewModel.getLiveRefreshStatus().observe(TicketValidationActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean refreshStatus) {
                if (mProgressDialog == null) {
                    mProgressDialog = AppUtil.createProgress(TicketValidationActivity.this);
                }
                if (refreshStatus) {
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }
            }
        });

        mRlBaseView = findViewById(R.id.activity_ticket_validation_rl_base_view);

        mSvCameraView = findViewById(R.id.activity_ticket_validation_sv_camera_view);

        mTvScannedValue = findViewById(R.id.activity_ticket_validation_tv_scanned_value);

        mIvRefresh = findViewById(R.id.activity_ticket_validation_iv_refresh);
        mIvRefresh.setOnClickListener(this);
        mIvFlash = findViewById(R.id.activity_ticket_validation_iv_flash);
        mIvFlash.setOnClickListener(this);

    }

    //Toolbar back button pressed
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_ticket_validation_iv_refresh:
                clearScannedData();
                break;
            case R.id.activity_ticket_validation_iv_flash:
                flashOnButton();
                break;
            default:
                break;
        }
    }

    // setting up camera to scan barcode or QR
    private void setUpScanner() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        mCameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setAutoFocusEnabled(true)
                .build();

        Log.d(TAG, "+---------> setUpQRScanner <---------+");
        mSvCameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    Log.d(TAG, "+---------> surfaceCreated <---------+");
                    if (ActivityCompat.checkSelfPermission(TicketValidationActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                    Log.d(TAG, "+---------> start <---------+");
                    mCameraSource.start(mSvCameraView.getHolder());

                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    mTvScannedValue.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                clearScannedData();
                                String scannedValue = barcodes.valueAt(0).displayValue;
                                mCameraSource.stop();

                                ScannedTicket scannedTicket = new Gson().fromJson(scannedValue, ScannedTicket.class);
                                if (scannedTicket != null &&
                                        scannedTicket.getTicketId() != null && !scannedTicket.getTicketId().isEmpty() &&
                                        scannedTicket.getTheatreSessionId() != null && !scannedTicket.getTheatreSessionId().isEmpty()) {

                                    showScannedTicketDetails(scannedTicket);
                                } else {
                                    showScannedTicketIssue();
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                showScannedTicketIssue();
                            }
                        }
                    });
                }
            }
        });
    }

    private void showScannedTicketDetails(final ScannedTicket scannedTicket) {
        showedCount = showedCount + 1;
        final AlertDialog dialog = new AlertDialog.Builder(TicketValidationActivity.this).create();
        AppUtil.showCustomConfirmAlert(dialog,
                TicketValidationActivity.this,
                getResources().getString(R.string.confirm_text),
                "Scanned Ticket ID: " + scannedTicket.getTicketId() + "\nScanned Theatre Session ID: " + scannedTicket.getTheatreSessionId() + "\nDo you want to validate?",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showedCount = showedCount - 1;
                        dialog.dismiss();
                        if (showedCount == 0) {

                            mViewModel.validateScannedTicket(scannedTicket);
                            mViewModel.getLiveTicketValidationProcessResult().observe(TicketValidationActivity.this, new Observer<ProcessResult<String>>() {
                                @Override
                                public void onChanged(ProcessResult<String> result) {
                                    if (result != null) {
                                        if (result.getProcessStatus().equals(AppConst.PROCESS_RESULT_STATUS_SUCCESS)) {
                                            showMessage("SUCCESS", result.getResult(), true);
                                        } else if (result.getProcessStatus().equals(AppConst.PROCESS_RESULT_STATUS_UN_SUCCESS)) {
                                            showMessage("ERROR", result.getMessage(), false);
                                        }
                                    } else {
                                        showMessage("ERROR", "Error in the ticket validation process\nPlease try again", false);
                                    }
                                }
                            });

                        }
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showedCount = showedCount - 1;
                        dialog.dismiss();
                        if (showedCount == 0) {
                            clearScannedData();
                        }
                    }
                },
                getResources().getString(R.string.yes_text),
                getResources().getString(R.string.no_text), false);
    }

    private void showMessage(String title, String message, boolean isSuccess) {
        final AlertDialog dialog = new AlertDialog.Builder(TicketValidationActivity.this).create();
        AppUtil.showCustomStandardAlert(dialog,
                TicketValidationActivity.this,
                title,
                message,
                isSuccess ? getResources().getDrawable(R.drawable.ic_check) : getResources().getDrawable(R.drawable.ic_close),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        clearScannedData();
                    }
                },
                getResources().getString(R.string.ok_text),
                false);
    }

    private void showScannedTicketIssue() {
        showedCount = showedCount + 1;
        final AlertDialog dialog = new AlertDialog.Builder(TicketValidationActivity.this).create();
        AppUtil.showCustomStandardAlert(dialog,
                TicketValidationActivity.this,
                getResources().getString(R.string.error_text),
                "Issue with the scanned QR/Bar code format",
                getResources().getDrawable(R.drawable.ic_close),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showedCount = showedCount - 1;
                        dialog.dismiss();
                        if (showedCount == 0) {
                            clearScannedData();
                        }
                    }
                },
                getResources().getString(R.string.ok_text),
                false);
    }

    // switch on / off flash light
    private void flashOnButton() {
        mCamera = getCamera(mCameraSource);
        if (mCamera != null) {
            try {
                android.hardware.Camera.Parameters param = mCamera.getParameters();
                param.setFlashMode(!mFlashMode ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(param);
                mFlashMode = !mFlashMode;
                if (mFlashMode) {
                    Toast.makeText(TicketValidationActivity.this, "Flash On", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TicketValidationActivity.this, "Flash Off", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void clearScannedData() {
        /*mTvScannedCode.setText("");
        mTvScannedCode.setHint("Scanned Code");*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            mCameraSource.start(mSvCameraView.getHolder());
        } catch (Exception ex) {
            ex.printStackTrace();
            Snackbar.make(mRlBaseView, "Camera is not working", Snackbar.LENGTH_LONG).show();
        }
    }

    // get camera from camera source
    private static Camera getCamera(@NonNull CameraSource cameraSource) {
        Field[] declaredFields = CameraSource.class.getDeclaredFields();

        for (Field field : declaredFields) {
            if (field.getType() == Camera.class) {
                field.setAccessible(true);
                try {
                    Camera camera = (Camera) field.get(cameraSource);
                    if (camera != null) {
                        return camera;
                    }
                    return null;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return null;
    }

}
