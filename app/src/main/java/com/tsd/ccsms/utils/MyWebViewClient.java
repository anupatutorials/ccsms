package com.tsd.ccsms.utils;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class MyWebViewClient extends WebViewClient {

    private LinearLayout mLlProgressBar;

    public MyWebViewClient(LinearLayout llProgressBar) {
        this.mLlProgressBar = llProgressBar;

        mLlProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);

        mLlProgressBar.setVisibility(View.GONE);
    }
}