package com.tsd.ccsms.utils;

import android.app.Activity;

public class UserInfoManager {

    public static void logout(Activity act) {
        try {
            String existingAccountNumber = SharedPref.getString(act, SharedPref.ACCOUNT_NUMBER_KEY, "");
            SharedPref.getPref(act).edit().clear().commit();
            SharedPref.saveString(act, SharedPref.ACCOUNT_NUMBER_KEY, existingAccountNumber);
        } catch (Exception e) {
        }
    }

    public static boolean isSignedIn(Activity act) {
        return SharedPref.getBoolean(act,
                SharedPref.VERIFIED_LOGGED, false);

    }

}
