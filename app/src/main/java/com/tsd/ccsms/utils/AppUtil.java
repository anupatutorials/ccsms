package com.tsd.ccsms.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.tsd.ccsms.R;
import com.tsd.ccsms.activities.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

public class AppUtil {

    // progress bar handling - Create
    public static Dialog createProgress(Activity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(0));
        dialog.setContentView(R.layout.fragment_dialog_progress);
        ProgressBar progressBar = dialog.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(activity.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        dialog.setCancelable(false);
        return dialog;
    }

    // progress bar handling
    public static Dialog showProgress(Activity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(0));
        dialog.setContentView(R.layout.fragment_dialog_progress);
        ProgressBar progressBar = dialog.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(activity.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    /* // Show Dark dialog curtain
    public static Dialog showCurtain(Activity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(0));
        dialog.setContentView(R.layout.fragment_dialog_curtain);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }*/

    // Round off to two decimal places
    public static double roundOff(double value) {
        DecimalFormat df = new DecimalFormat("###.##");
        return Double.parseDouble(df.format(value));
    }

    // get unique device ID (Android ID)
    public static String getDeviceId(Context mContext) {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    // land to settings
    public static void showSystemSettingsDialog(Context context) {
        context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
    }

    // Land/Open application package settings
    public static void openPackageSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    // Check network connection availability
    public static boolean checkNetworkConnection(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }

        NetworkInfo activeNetworks = connectivityManager.getActiveNetworkInfo();
        if (activeNetworks != null && activeNetworks.isConnected()) {
            return activeNetworks.isConnectedOrConnecting();
        }
        return false;
    }

    // hide keyboard
    public static void hideDefaultKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    // Disable Touch events
    public static void disableTouchEvents(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    // Enable Touch events
    public static void enableTouchEvents(Activity activity) {
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    // start an activity without intentExtras
    public static void startActivity(Activity activity, Class<? extends Activity> aClass) {
        Intent intent = new Intent(activity, aClass);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // start Activity with intentExtras
    public static void startActivityWithExtra(Activity activity, Intent intent) {
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // Start Activity with intentExtras and request code
    public static void startActivityForResult(Activity activity, Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // start an activity using drawer
    public static void startActivityFromDrawer(Activity activity, Class<? extends Activity> aClass) {
        if (!activity.getClass().getSimpleName().equals(MainActivity.class.getSimpleName()))
            activity.finish();
        Intent intent = new Intent(activity, aClass);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // custom alert dialog box (Updated UI)
    public static void showCustomStandardAlert(final AlertDialog dialog, Activity activity, String title, String message, Drawable imageIconResource,
                                               View.OnClickListener buttonClickListener, String buttonText, boolean cancelableState) {

        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_custom_standard_alert, null);
        dialogView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); // This line is for hardware acceleration false
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setView(dialogView);
        dialog.setCancelable(cancelableState);

        TextView tvTitle = dialogView.findViewById(R.id.custom_alert_tv_title);
        TextView tvMessage = dialogView.findViewById(R.id.custom_alert_tv_description);
        ImageView ivIcon = dialogView.findViewById(R.id.custom_alert_iv_icon);
        ivIcon.setBackground(imageIconResource);
        Button btnCancel = dialogView.findViewById(R.id.custom_alert_btn_text);
        btnCancel.setOnClickListener(buttonClickListener);

        if (buttonClickListener == null)
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        else {
            btnCancel.setOnClickListener(buttonClickListener);
        }

        if (title.isEmpty()) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setText(title);
        }

        tvMessage.setText(message);
        btnCancel.setText(buttonText);

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        Log.i("Screen Width: ", "" + width);

        dialog.show();
        dialog.getWindow().setLayout(width - 80, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    // confirm alert dialog box (Updated UI)
    public static void showCustomConfirmAlert(final AlertDialog dialog, Activity activity, String title, String message,
                                              View.OnClickListener yesClickListener, View.OnClickListener noClickListener,
                                              String yesText, String noText, boolean cancelableState) {

        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_custom_confirm_alert, null);
        dialogView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); // This line is for hardware acceleration false
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setView(dialogView);
        dialog.setCancelable(cancelableState);

        TextView tvTitle = dialogView.findViewById(R.id.custom_alert_tv_title);
        TextView tvMessage = dialogView.findViewById(R.id.custom_alert_tv_description);
        Button btnCancel = dialogView.findViewById(R.id.custom_alert_btn_cancel);
        Button btnSettings = dialogView.findViewById(R.id.custom_alert_btn_settings);
        btnCancel.setOnClickListener(noClickListener);
        btnSettings.setOnClickListener(yesClickListener);

        if (noClickListener == null)
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        if (yesClickListener == null)
            btnSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        if (title.isEmpty()) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setText(title);
        }

        tvMessage.setText(message);
        btnCancel.setText(noText);
        btnSettings.setText(yesText);

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        Log.i("Screen Width: ", "" + width);

        dialog.show();
        dialog.getWindow().setLayout(width - 80, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    // confirm alert dialog box with starting position check box (Updated UI)
    public static void showCustomConfirmWithLocationAlert(final AlertDialog dialog, Activity activity,
                                                          String title, String message,
                                                          View.OnClickListener yesClickListener, View.OnClickListener noClickListener,
                                                          CompoundButton.OnCheckedChangeListener checkBoxClickListener,
                                                          String yesText, String noText, String checkBoxText, boolean cancelableState) {

        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_custom_confirm_with_location_alert, null);
        dialogView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); // This line is for hardware acceleration false
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setView(dialogView);
        dialog.setCancelable(cancelableState);

        TextView tvTitle = dialogView.findViewById(R.id.custom_alert_tv_title);
        TextView tvMessage = dialogView.findViewById(R.id.custom_alert_tv_description);
        CheckBox startingFromHomeCheckBox = dialogView.findViewById(R.id.starting_from_home_checkbox);
        Button btnCancel = dialogView.findViewById(R.id.custom_alert_btn_cancel);
        Button btnSettings = dialogView.findViewById(R.id.custom_alert_btn_settings);
        btnCancel.setOnClickListener(noClickListener);
        btnSettings.setOnClickListener(yesClickListener);
        startingFromHomeCheckBox.setOnCheckedChangeListener(checkBoxClickListener);

        if (noClickListener == null)
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        if (yesClickListener == null)
            btnSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        if (title.isEmpty()) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setText(title);
        }

        tvMessage.setText(message);
        btnCancel.setText(noText);
        btnSettings.setText(yesText);
        startingFromHomeCheckBox.setText(checkBoxText);

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        Log.i("Screen Width: ", "" + width);

        dialog.show();
        dialog.getWindow().setLayout(width - 80, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    /*
    // custom scrollable text-view alert dialog box (Updated UI)
    public static void showCustomScrollableAlert(final AlertDialog dialog, Activity activity, String title, String message,
                                                 Drawable imageIconResource, View.OnClickListener buttonClickListener,
                                                 String buttonText, boolean cancelableState) {
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_custom_scrollable_text_view_alert, null);
        dialogView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); // This line is for hardware acceleration false
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setView(dialogView);
        dialog.setCancelable(cancelableState);

        TextView tvTitle = dialogView.findViewById(R.id.dialog_custom_scrollable_text_view_alert_tv_title_text);
        TextView tvMessage = dialogView.findViewById(R.id.dialog_custom_scrollable_text_view_alert_tv_message);
        ImageView ivIcon = dialogView.findViewById(R.id.dialog_custom_scrollable_text_view_alert_iv_icon);
        Button btnButton = dialogView.findViewById(R.id.dialog_custom_scrollable_text_view_alert_btn_button);
        btnButton.setOnClickListener(buttonClickListener);

        if (buttonClickListener == null)
            btnButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        ivIcon.setBackground(imageIconResource);
        tvTitle.setText(title);
        tvMessage.setText(message);
        btnButton.setText(buttonText);

        UiUtil.setTextViewFontSizeBasedOnScreenDensity(activity, tvTitle, 60.0, true);
        UiUtil.setButtonFontSizeBasedOnScreenDensity(activity, btnButton, 63.0, Typeface.BOLD);

        dialog.show();
        dialog.getWindow().setLayout(660, WindowManager.LayoutParams.WRAP_CONTENT);
    }*/

    /*public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    // String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    String text = tv.getText().toString();
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    *//*tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);*//*
                    tv.setText(tv.getText().toString());
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {

            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Read less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. Read more", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }*/

    public static void openPlayStore(Context context, String appPackageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static boolean checkApplicationExists(Context context, String appPackageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appPackageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public static Long getApplicationInstalledVersionCode(Context context, String appPackageName) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(appPackageName, PackageManager.GET_ACTIVITIES);
            return info.getLongVersionCode();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getToday() {
        // format date as -> 16/11/2016 (Date/Month/Year)
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String formatDate(Long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        return dateFormat.format(date);
    }

    public static String formatTime(Long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(date);
    }

    // Split the string using splitUsing character and send the formatted string
    public static String getFormattedString(String fullStringText, String splitUsing) {
        try {
            String[] splitString = fullStringText.split(splitUsing);
            String formattedString = "";
            for (int i = 0; i < splitString.length; i++) {
                formattedString = formattedString + splitString[i] + "\n";
            }
            return formattedString;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    // Read arrays safely
    public static <T> List<T> safeClient(List<T> other) {
        return other == null ? Collections.EMPTY_LIST : other;
    }

    /*
     * Below method used to remove the folder and folder contains data(Ex: PDF, Images, Etc:)
     * */
    // Remove directory (Folder)
    public static boolean removeDirectory(File directory) {

        if (directory == null)
            return false;
        if (!directory.exists())
            return true;
        if (!directory.isDirectory())
            return false;

        String[] list = directory.list();

        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                File entry = new File(directory, list[i]);

                if (entry.isDirectory()) {
                    if (!removeDirectory(entry))
                        return false;
                } else {
                    if (!entry.delete())
                        return false;
                }
            }
        }

        return directory.delete();
    }

    // Create base64 string using image (using image path)
    public static String createBase64String(String imagePath) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        // Bitmap bitmap = BitmapFactory.decodeResource(getResources, ResourceID);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageBase64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return imageBase64String;
    }

    /*
     * We can use following method to create base64 String using a file (.pdf , .docx Etc:)
     * */
    public static String convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 11];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();

            Log.e("Byte array", ">" + byteArray);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    /*
     * We can use following method to create a file using base64 String
     * */
    public static File createFileUsingString(String stringFile) {
        // Currently we create the File inside this method

        /*File filesDir = new File(Environment.getExternalStorageDirectory(), "M.SMS/" + mSvoItem.getSvoName());

        // Create the storage directory if it does not exist
        if (!filesDir.exists()) {
            if (!filesDir.mkdirs()) {
                Log.e(TAG, "Failed to create directory");
            }
        }
        File downloadedFile = new File(filesDir, mSvoItem.getSvoName() + "_Created_by_Anupa" + ".pdf");*/
        try {
            File filesDir = new File(Environment.getExternalStorageDirectory(), "CreatedFile/");
            // Create the storage directory if it does not exist
            if (!filesDir.exists()) {
                if (!filesDir.mkdirs()) {
                    Log.e(TAG, "Failed to create directory");
                }
            }
            File createdFile = new File(filesDir, "Created_by_Anupa_" + System.currentTimeMillis() + ".pdf"); // File extension can change
            byte[] pdfAsBytes = Base64.decode(stringFile, 0);
            FileOutputStream os;
            os = new FileOutputStream(createdFile, false);
            os.write(pdfAsBytes);
            os.flush();
            os.close();

            return createdFile;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
