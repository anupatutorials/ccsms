package com.tsd.ccsms.utils;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.tsd.ccsms.config.AppConst;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 11-Dec-17.
 */

public class WebUtil {

    // retrofit instances
    private static Retrofit mRetrofitInstanceForUserRoles = null;
    private static Retrofit mRetrofitInstance = null;

    /**
     * Create and return Retrofit instance.
     *
     * @return mRetrofitInstance object
     */
    public static Retrofit getRetrofitInstanceForUserRoles(String baseUrl) {

        if (mRetrofitInstanceForUserRoles == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addNetworkInterceptor((Interceptor) new StethoInterceptor())
                    .readTimeout(AppConst.DATA_TIMEOUT, TimeUnit.SECONDS) // socket timeout
                    .connectTimeout(AppConst.CONNECTION_TIMEOUT, TimeUnit.SECONDS) // connect timeout
                    .build();

            mRetrofitInstanceForUserRoles = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }
        return mRetrofitInstanceForUserRoles;
    }

    public static Retrofit getRetrofitInstance(String baseUrl) {

        if (mRetrofitInstance == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addNetworkInterceptor((Interceptor) new StethoInterceptor())
                    .readTimeout(AppConst.DATA_TIMEOUT, TimeUnit.SECONDS) // socket timeout
                    .connectTimeout(AppConst.CONNECTION_TIMEOUT, TimeUnit.SECONDS) // connect timeout
                    .build();

            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }
        return mRetrofitInstance;
    }

    public static String createAuthString(Context context, String userEmail, String userPassword) {

        String accountNumber = SharedPref.getString(context, SharedPref.ACCOUNT_NUMBER_KEY, SharedPref.DEFAULT_VALUE_FOR_STRING);
        String nLAuth = "NLAuth nlauth_account=" + accountNumber + ",nlauth_email=" + userEmail + ",nlauth_signature=" + userPassword;
        return nLAuth;

    }

    public static String getAuthString(Context context) {
        String accountNumber = SharedPref.getString(context, SharedPref.ACCOUNT_NUMBER_KEY, SharedPref.DEFAULT_VALUE_FOR_STRING);
        String userEmail = SharedPref.getString(context, SharedPref.USERNAME_KEY, SharedPref.DEFAULT_VALUE_FOR_STRING);
        String userPassword = SharedPref.getString(context, SharedPref.PASSWORD_KEY, SharedPref.DEFAULT_VALUE_FOR_STRING);
        String roleId = SharedPref.getUserRole(context).getRole().getInternalId();

        String nLAuth = "NLAuth nlauth_account=" + accountNumber + ",nlauth_email=" + userEmail + "" +
                ",nlauth_signature=" + userPassword + ",nlauth_role=" + roleId;
        return nLAuth;
    }

}
