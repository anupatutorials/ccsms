package com.tsd.ccsms.dtos;

public class DataCenterURLs {

    private String webservicesDomain, restDomain, systemDomain;

    public DataCenterURLs(String webservicesDomain, String restDomain, String systemDomain) {
        this.webservicesDomain = webservicesDomain;
        this.restDomain = restDomain;
        this.systemDomain = systemDomain;
    }

    public DataCenterURLs() {

    }

    public String getWebservicesDomain() {
        return webservicesDomain;
    }

    public void setWebservicesDomain(String webservicesDomain) {
        this.webservicesDomain = webservicesDomain;
    }

    public String getRestDomain() {
        return restDomain;
    }

    public void setRestDomain(String restDomain) {
        this.restDomain = restDomain;
    }

    public String getSystemDomain() {
        return systemDomain;
    }

    public void setSystemDomain(String systemDomain) {
        this.systemDomain = systemDomain;
    }

}
