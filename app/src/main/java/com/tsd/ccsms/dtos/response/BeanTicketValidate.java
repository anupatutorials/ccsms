package com.tsd.ccsms.dtos.response;

public class BeanTicketValidate {

/*
{
    "isSuccess": true,
    "errorMessage": ""
}
*/

    private boolean isSuccess;
    private String errorMessage;

    public BeanTicketValidate() {
    }

    public BeanTicketValidate(boolean isSuccess, String errorMessage) {
        this.isSuccess = isSuccess;
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
