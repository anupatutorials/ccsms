package com.tsd.ccsms.dtos;

public class ScannedTicket {

/*
{
"ticketId": "CN200201ODF1",
"theatreSessionId": "2"
}
*/

    private String ticketId, theatreSessionId;

    public ScannedTicket() {
    }

    public ScannedTicket(String ticketId, String theatreSessionId) {
        this.ticketId = ticketId;
        this.theatreSessionId = theatreSessionId;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTheatreSessionId() {
        return theatreSessionId;
    }

    public void setTheatreSessionId(String theatreSessionId) {
        this.theatreSessionId = theatreSessionId;
    }

}
