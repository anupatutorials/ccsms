package com.tsd.ccsms.dtos;

public class UserRole {

    private Account account;
    private Role role;
    private DataCenterURLs dataCenterURLs;

    public UserRole(Account account, Role role, DataCenterURLs dataCenterURLs) {
        this.account = account;
        this.role = role;
        this.dataCenterURLs = dataCenterURLs;
    }

    public UserRole() {

    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public DataCenterURLs getDataCenterURLs() {
        return dataCenterURLs;
    }

    public void setDataCenterURLs(DataCenterURLs dataCenterURLs) {
        this.dataCenterURLs = dataCenterURLs;
    }
}
