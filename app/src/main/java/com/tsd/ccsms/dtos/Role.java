package com.tsd.ccsms.dtos;

public class Role {

    private String internalId, name;

    public Role(String internalId, String name) {
        this.internalId = internalId;
        this.name = name;
    }

    public Role() {
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
