package com.tsd.ccsms.dtos;

public class Account {

    private String internalId, name;

    public Account(String internalId, String name) {
        this.internalId = internalId;
        this.name = name;
    }

    public Account() {
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
