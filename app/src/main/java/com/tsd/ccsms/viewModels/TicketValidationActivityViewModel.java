package com.tsd.ccsms.viewModels;

import android.app.Activity;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.perf.metrics.AddTrace;
import com.tsd.ccsms.config.AppConst;
import com.tsd.ccsms.dtos.ProcessResult;
import com.tsd.ccsms.dtos.ScannedTicket;
import com.tsd.ccsms.dtos.response.BeanTicketValidate;
import com.tsd.ccsms.interfaces.listeners.RxResponseListener;
import com.tsd.ccsms.retrofit.TicketValidateRx;

import retrofit2.Response;

public class TicketValidationActivityViewModel extends ViewModel {

    // Constants
    public static final String TAG = TicketValidationActivityViewModel.class.getSimpleName();

    // Objects
    private Activity mActivity;
    private MutableLiveData<Boolean> mLiveRefreshStatus;
    private MutableLiveData<ProcessResult<String>> mLiveTicketValidationProcessResult;

    public TicketValidationActivityViewModel(Activity activity) {
        this.mActivity = activity;
    }

    public void initViewModel() {
        mLiveRefreshStatus = new MutableLiveData<>();
        mLiveRefreshStatus.setValue(false);
    }

    public MutableLiveData<Boolean> getLiveRefreshStatus() {
        return mLiveRefreshStatus;
    }

    @AddTrace(name = "validateScannedTicket", enabled = true)
    public void validateScannedTicket(ScannedTicket scannedTicket) {
        try {
            mLiveRefreshStatus.postValue(true);
            mLiveTicketValidationProcessResult = new MutableLiveData<>();

            callTicketValidateRxWay(scannedTicket);
        } catch (Exception ex) {
            ex.printStackTrace();
            ProcessResult<String> result = new ProcessResult<>(AppConst.PROCESS_RESULT_ACTION_VALIDATE_TICKET, AppConst.PROCESS_RESULT_STATUS_UN_SUCCESS,
                    "System Error:\n" + ex.getMessage(), null);
            mLiveTicketValidationProcessResult.postValue(result);
            mLiveRefreshStatus.postValue(false);
        }
    }

    private void callTicketValidateRxWay(ScannedTicket scannedTicket) {

        final BeanTicketValidate[] beanTicketValidateResponse = new BeanTicketValidate[1];

        new TicketValidateRx(mActivity, scannedTicket, new RxResponseListener<BeanTicketValidate>() {
            @Override
            public void serviceResponse(Response<BeanTicketValidate> serviceResponseResponse) {
                if (serviceResponseResponse != null && serviceResponseResponse.isSuccessful()) {
                    if (serviceResponseResponse.body() != null) {
                        beanTicketValidateResponse[0] = serviceResponseResponse.body();
                    } else {
                        // NULL Response body
                        ProcessResult<String> result = new ProcessResult<>(AppConst.PROCESS_RESULT_ACTION_VALIDATE_TICKET, AppConst.PROCESS_RESULT_STATUS_UN_SUCCESS,
                                "Error with the response\nPlease try again", null);
                        mLiveTicketValidationProcessResult.postValue(result);
                    }
                } else {
                    // NULL serviceResponseResponse OR serviceResponseResponse NOT success
                    ProcessResult<String> result = new ProcessResult<>(AppConst.PROCESS_RESULT_ACTION_VALIDATE_TICKET, AppConst.PROCESS_RESULT_STATUS_UN_SUCCESS,
                            "Error with the response\nPlease try again", null);
                    mLiveTicketValidationProcessResult.postValue(result);
                }

            }

            @Override
            public void serviceThrowable(Throwable throwable) {
                Log.e(TAG, throwable.toString(), throwable);
                mLiveRefreshStatus.postValue(false);
            }

            @Override
            public void serviceComplete() {
                if (beanTicketValidateResponse[0] != null) {
                    if (beanTicketValidateResponse[0].isSuccess()) {
                        // Show valid ticket message
                        ProcessResult<String> result = new ProcessResult<>(AppConst.PROCESS_RESULT_ACTION_VALIDATE_TICKET, AppConst.PROCESS_RESULT_STATUS_SUCCESS,
                                AppConst.PROCESS_RESULT_STATUS_SUCCESS, "Ticket validation Success");
                        mLiveTicketValidationProcessResult.postValue(result);
                    } else {
                        // Show Invalid ticket error message
                        ProcessResult<String> result = new ProcessResult<>(AppConst.PROCESS_RESULT_ACTION_VALIDATE_TICKET, AppConst.PROCESS_RESULT_STATUS_UN_SUCCESS,
                                beanTicketValidateResponse[0].getErrorMessage(), null);
                        mLiveTicketValidationProcessResult.postValue(result);
                    }
                }
                mLiveRefreshStatus.postValue(false);
            }
        }).callServiceRXWay();
    }

    public MutableLiveData<ProcessResult<String>> getLiveTicketValidationProcessResult() {
        return mLiveTicketValidationProcessResult;
    }

}
