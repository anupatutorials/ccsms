package com.tsd.ccsms.viewModels;

import android.Manifest;
import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivityViewModel extends ViewModel {

    // Constants
    public static final String TAG = MainActivityViewModel.class.getSimpleName();

    // Objects
    private Activity mActivity;
    private MutableLiveData<Boolean> mLiveCheckCameraPermission;

    public MainActivityViewModel(Activity activity) {
        this.mActivity = activity;
    }

    //Request Call Phone Permission
    public void requestRuntimePermissionForAccessCamera() {
        mLiveCheckCameraPermission = new MutableLiveData<>();
        Dexter.withActivity(mActivity)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        mLiveCheckCameraPermission.postValue(true);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            mLiveCheckCameraPermission.postValue(false);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public LiveData<Boolean> getLiveCheckCameraPermission() {
        return mLiveCheckCameraPermission;
    }
}
