package com.tsd.ccsms.viewModels;

import android.Manifest;
import android.app.Activity;
import android.os.Handler;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tsd.ccsms.config.AppConst;

public class SplashActivityViewModel extends ViewModel {

    // Constants
    private static final String TAG = SplashActivityViewModel.class.getSimpleName();

    // Objects
    private Activity mActivity;
    private boolean mShowedSettings = false;
    private MutableLiveData<Boolean> mLiveIsSignedIn, mLiveShowSettings;

    public SplashActivityViewModel(Activity activity) {
        mActivity = activity;
    }

    public void initViewModel() {
        mLiveIsSignedIn = new MutableLiveData<>();
        mLiveShowSettings = new MutableLiveData<>();
    }

    public void requestMultiplePermissions() {
        if (!mShowedSettings) {
            Dexter.withActivity(mActivity)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            proceed();
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            if(response.isPermanentlyDenied()){
                                mLiveShowSettings.setValue(true);
                                mShowedSettings = true;
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .onSameThread()
                    .check();

        } else {
            mShowedSettings = false;
            mLiveShowSettings.postValue(true);
        }
    }

    private void proceed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                mLiveIsSignedIn.postValue(true);
            }
        }, AppConst.SPLASH_TIME_OUT);
    }

    public LiveData<Boolean> getLiveIsSignedIn() {
        return mLiveIsSignedIn;
    }

    public LiveData<Boolean> getLiveShowSettings() {
        return mLiveShowSettings;
    }
}
