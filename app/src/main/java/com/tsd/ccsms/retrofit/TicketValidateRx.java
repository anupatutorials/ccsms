package com.tsd.ccsms.retrofit;

import android.content.Context;
import android.util.ArrayMap;
import android.util.Log;

import com.tsd.ccsms.BuildConfig;
import com.tsd.ccsms.config.ApiConst;
import com.tsd.ccsms.config.AppConst;
import com.tsd.ccsms.dtos.ScannedTicket;
import com.tsd.ccsms.dtos.response.BeanTicketValidate;
import com.tsd.ccsms.interfaces.RxInterface;
import com.tsd.ccsms.interfaces.listeners.RxResponseListener;
import com.tsd.ccsms.utils.WebUtil;

import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TicketValidateRx {

    // constants
    private static final String TAG = TicketValidateRx.class.getSimpleName();

    //response listener
    private RxResponseListener mRxResponseListener;

    private Context mContext;
    private ScannedTicket mScannedTicket;

    public TicketValidateRx(Context context, ScannedTicket scannedTicket, RxResponseListener rxResponseListener) {
        this.mContext = context;
        this.mScannedTicket = scannedTicket;
        this.mRxResponseListener = rxResponseListener;
    }

    public void callServiceRXWay() {

        String baseURL;
        if (BuildConfig.DEBUG) {
            baseURL = AppConst.SANDBOX_BASE_URL;
        } else {
            baseURL = AppConst.PRODUCTION_BASE_URL;
        }

        RxInterface rXService = WebUtil.getRetrofitInstance(baseURL).create(RxInterface.class);

        //String nLAuth = WebUtil.getAuthString(mContext);

        Map<String, String> queryMap = new ArrayMap<>(2);
        queryMap.put(ApiConst.ATTR_TAG_TICKET_ID_KET, mScannedTicket.getTicketId());
        queryMap.put(ApiConst.ATTR_TAG_THEATRE_SESSION_ID, mScannedTicket.getTheatreSessionId());

        Log.d(TAG, "--------" + baseURL);

        Observable<Response<BeanTicketValidate>> observable = rXService.validateTicket(queryMap);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Action1<Response<BeanTicketValidate>>() {
                               @Override
                               public void call(Response<BeanTicketValidate> serviceResponseResponse) {
                                   mRxResponseListener.serviceResponse(serviceResponseResponse);
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                mRxResponseListener.serviceThrowable(throwable);
                            }
                        },
                        new Action0() {
                            @Override
                            public void call() {
                                mRxResponseListener.serviceComplete();
                            }
                        });
    }

}