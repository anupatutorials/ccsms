package com.tsd.ccsms.config;

public class ApiConst {

    // API Attribute keys
    public static final String ATTR_TAG_SCRIPT_KEY = "script";
    public static final String ATTR_TAG_DEPLOY_KEY = "deploy";

    public static final String ATTR_TAG_TICKET_ID_KET = "TicketId";
    public static final String ATTR_TAG_THEATRE_SESSION_ID = "TheatreSessionId";
}
