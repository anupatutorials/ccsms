package com.tsd.ccsms.config;

public class AppConst {

    // Sandbox URL for fetch user roles
    public static final String SANDBOX_BASE_URL = "http://api.securedserver.xyz";
    // Production URL for fetch user roles
    public static final String PRODUCTION_BASE_URL = "http://api.securedserver.xyz";

    public static final String PREFIX = "";
    public static final String CONTENT_TYPE = "application/json";

    public static final int CONNECTION_TIMEOUT = 120000; // 120 seconds
    public static final int DATA_TIMEOUT = 120000; // 120 seconds
    public static final int RESPONSE_SUCCESS_CODE = 200;
    public static final int SPLASH_TIME_OUT = 2000; // Splash screen time out
    public static final int BACK_PRESSED_DELAY = 2000; // onBackPressed()-> delay

    // Sandbox Script ID's (Endpoints)
    public static final String SANDBOX_DEPLOY = "1";

    // Production Script ID's (Endpoints)
    public static final String PRODUCTION_DEPLOY = "1";

    // Navigation Drawer menu items
    public static final String NAV_ITEM_DASHBOARD = "Dashboard";
    public static final String NAV_ITEM_TICKET_VALIDATION = "Ticket Validation";
    public static final String NAV_ITEM_ABOUT = "About";
    /*public static final String NAV_ITEM_Logout = "Logout";*/

    // Process Result Actions
    public static final String PROCESS_RESULT_ACTION_VALIDATE_TICKET = "action_login";

    // Process Result Statuses
    public static final String PROCESS_RESULT_STATUS_SUCCESS = "SUCCESS";
    public static final String PROCESS_RESULT_STATUS_UN_SUCCESS = "UN-SUCCESS";

}
