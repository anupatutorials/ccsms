package com.tsd.ccsms.interfaces;

import com.tsd.ccsms.config.AppConst;
import com.tsd.ccsms.dtos.response.BeanTicketValidate;

import java.util.Map;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface RxInterface {

    // EP. 1 - Ticket Validation
    @GET(AppConst.PREFIX + "/api/Ticket/validate")
    Observable<Response<BeanTicketValidate>> validateTicket(/*@Header("Content-Type") String contentType, */@QueryMap Map<String, String> params);

}
