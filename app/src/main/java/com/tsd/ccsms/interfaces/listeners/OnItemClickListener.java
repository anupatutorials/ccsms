package com.tsd.ccsms.interfaces.listeners;

public interface OnItemClickListener<T> {

    void onItemClick(int position, T data);

}